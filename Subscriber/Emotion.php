<?php
/**
 * Created by PluginAuthor
 */


namespace PluginName\Subscriber;

use Doctrine\Common\Collections\ArrayCollection;
use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Enlight_Event_EventArgs;
use Shopware\Components\Theme\LessDefinition;
use Shopware_Controllers_Widgets_Emotion;

class Emotion implements  SubscriberInterface
{

    public static function getSubscribedEvents ()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Widgets_Emotion' => 'extendsEmotionTemplates',
            'Theme_Compiler_Collect_Plugin_Less' => 'onCollectLessFiles',
            'Shopware_Controllers_Widgets_Emotion_AddElement' => 'modifyEmotionData',
        ];
    }


    public function extendsEmotionTemplates ( Enlight_Controller_ActionEventArgs $args ): void
    {
        /** @var Shopware_Controllers_Widgets_Emotion $subject */
        $controller = $args->getSubject ();
        $view = $controller->View ();

        $view->addTemplateDir ( __DIR__ . '/../Resources/Views/emotion_components' );

        if ( $controller->Request ()->getModuleName () !== 'backend' ) {
            return;
        }

    }

    public function onCollectLessFiles ()
    {
        return new LessDefinition(
            [],
            [ __DIR__ . '/../Resources/Views/frontend/_public/src/less/all.less', ]
        );
    }

    /**
     * @param Enlight_Event_EventArgs $args
     * @return ArrayCollection
     */
    public function onCollectJavascriptFiles ( Enlight_Event_EventArgs $args )
    {
        $jsDir = __DIR__ . '/../Resources/Views/frontend/_public/src/js/global.js';
        return new ArrayCollection( array( $jsDir ) );
    }
}
