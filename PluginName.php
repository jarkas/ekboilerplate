<?php
/**
 * Created by PluginAuthor
 */

namespace PluginName;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use PluginName\Bootstrap\Installer;
use PluginName\Bootstrap\Updater;
use PluginName\Bootstrap\Uninstaller;

class PluginName extends Plugin
{
    /**
     * {@inheritdoc}
     */
    public function install(InstallContext $context)
    {
        $installer = new Installer(
            $this->getName (),
            $this->container->get ( 'shopware.emotion_component_installer' )
        );
        $installer->install ();
    }

    /**
     * {@inheritdoc}
     */
    public function activate(ActivateContext $context)
    {
        // on plugin activation clear the cache
        $context->scheduleClearCache(ActivateContext::CACHE_LIST_ALL);
    }


    /**
     * {@inheritdoc}
     */
    public function uninstall(UninstallContext $context)
    {
        $uninstaller = new Uninstaller();
        $uninstaller->uninstall($context);
        $context->scheduleClearCache(UninstallContext::CACHE_LIST_ALL);
    }

    /**
     * {@inheritdoc}
     */
    public function update(UpdateContext $context)
    {

        $updater = new Updater();
        $updater->update($context, $this->container->get('service_container'));

        $context->scheduleClearCache(UpdateContext::CACHE_LIST_ALL);
    }

    public function deactivate(DeactivateContext $context)
    {
        // on plugin deactivation clear the cache
        $context->scheduleClearCache(DeactivateContext::CACHE_LIST_ALL);
    }
}
