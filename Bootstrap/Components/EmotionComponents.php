<?php
/**
 * Created by PluginAuthor
 */

namespace PluginName\Bootstrap\Components;

use Shopware\Components\Emotion\ComponentInstaller;

class EmotionComponents
{
    /**
     * @var ComponentInstaller
     */
    private $emotionComponentInstaller;

    /**
     * @var string
     */
    private $pluginName;

    /**
     * @param string $pluginName
     * @param ComponentInstaller $emotionComponentInstaller
     */

    public function __construct ( $pluginName, ComponentInstaller $emotionComponentInstaller )
    {
        $this->emotionComponentInstaller = $emotionComponentInstaller;
        $this->pluginName = $pluginName;
    }

    public function install ()
    {
        $this->createEmotion ();
    }

    public function createEmotion ()
    {

        $component = $this->emotionComponentInstaller->createOrUpdate (
            $this->pluginName,
            'EmotionName',
            [
                'name' => 'EmotionName',
                'template' => 'component_emotion_name',
                'cls' => 'emotion_name',
                'description' => 'Emotion element boilerplate'
            ]
        );

        $component->createTextField (
            [
                'name' => 'EmotionNameText',
                'fieldLabel' => 'Emotion element boilerplate text',
                'allowBlank' => true,
                'translatable' => true
            ]
        );
    }
}
