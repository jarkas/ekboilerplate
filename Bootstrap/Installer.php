<?php
/**
 * Created by PluginAuthor
 */


namespace PluginName\Bootstrap;

use Shopware\Components\Emotion\ComponentInstaller;
use PluginName\Bootstrap\Components\EmotionComponents;

class Installer {
    /**
     * @var ComponentInstaller
     */
    private $emotionComponentInstaller;

    /**
     * @var string
     */
    private $pluginName;

    /**
     * @param string $pluginName
     * @param ComponentInstaller $emotionComponentInstaller
     */
    public function __construct ( $pluginName, ComponentInstaller $emotionComponentInstaller )
    {

        $this->emotionComponentInstaller = $emotionComponentInstaller;
        $this->pluginName = $pluginName;
    }

    public function install ()
    {
        $components = new EmotionComponents( $this->pluginName, $this->emotionComponentInstaller );
        $components->install ();
    }

}
