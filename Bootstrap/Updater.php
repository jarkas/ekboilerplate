<?php
/**
 * Created by PluginAuthor
 */

namespace PluginName\Bootstrap;

use Shopware\Components\Plugin\Context\UpdateContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Updater
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param UpdateContext $context
     * @param ContainerInterface $container
     */
    public function update ( UpdateContext $context, ContainerInterface $container ): void
    {
        $this->container = $container;

        if (version_compare($context->getCurrentVersion(), '1.0.0', '<=')) {
            $this->doSomething();

        }

    }

    private function doSomething()
    {
    }
}
