<?php
/**
 * Created by PluginAuthor
 */

namespace PluginName\Bootstrap;

use Shopware\Components\Plugin\Context\UninstallContext;

class Uninstaller
{

    /**
     * @param UninstallContext $uninstallContext
     */
    public function uninstall ( UninstallContext $uninstallContext ): void
    {
        if ( $uninstallContext->keepUserData () ) {
            return;
        }
    }
}

